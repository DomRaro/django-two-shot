from django.shortcuts import render, redirect
from .models import Receipt, ExpenseCategory, Account
from receipts.forms import ReceiptForm, ExpenseForm, AccountForm
from django.contrib.auth.decorators import login_required


@login_required
def receipt_list(request):
    receipt_list = Receipt.objects.filter(purchaser=request.user)
    context = {"receipt_list": receipt_list}

    return render(request, "receipts/receipt_list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()

    context = {
        "form": form,
    }
    return render(request, "receipts/create_receipt.html", context)


def categories_list(request):
    categories_list = ExpenseCategory.objects.filter(owner=request.user)
    context = {"categories_list": categories_list}

    return render(request, "receipts/categories_list.html", context)


@login_required
def create_categories(request):
    if request.method == "POST":
        form = ExpenseForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = ExpenseForm()

    context = {"form": form}
    return render(request, "receipts/create_categories.html", context)


def account_list(request):
    account_list = Account.objects.filter(owner=request.user)
    context = {"account_list": account_list}

    return render(request, "receipts/account_list.html", context)


def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()

    context = {"form": form}
    return render(request, "receipts/create_account.html", context)
