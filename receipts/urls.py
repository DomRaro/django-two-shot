from django.urls import path
from receipts.views import (
    receipt_list,
    create_receipt,
    account_list,
    categories_list,
    create_account,
    create_categories,
)


urlpatterns = [
    path("accounts/create/", create_account, name="create_account"),
    path("categories/create/", create_categories, name="create_category"),
    path("categories/", categories_list, name="category_list"),
    path("accounts/", account_list, name="account_list"),
    path("create/", create_receipt, name="create_receipt"),
    path("", receipt_list, name="home"),
]
